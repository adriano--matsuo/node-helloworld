module.exports = function(app) {
    let usersModel = {
        save: function(user, callback) {
            let db = app.config.db
            let connection = db.connect()

            connection.query('INSERT INTO users SET ?', user, function(err, result) {
                callback(err, result)
            });

            connection.end()
        },
        fetchAll: function(callback) {
            let db = app.config.db
            let connection = db.connect()

            connection.query('SELECT * FROM users', function(err, result) {
                callback(err, result)
            });

            connection.end()
        },
    }

    return usersModel
    
}
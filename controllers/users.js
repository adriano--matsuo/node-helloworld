module.exports = function(app) {
    let UsersController = {
        save: function(req, res) {
            let usersModel = app.models.users

            let user = {
                'name': req.body.name,
                'birthdate': req.body.birthdate,
                'active': req.body.active
            }

            usersModel.save(user, function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.write('Usuário gravado com sucesso')
                    res.end()
                }
            })

        },
        fetchAll: function(req, res) {
            let usersModel = app.models.users

            
            usersModel.fetchAll(function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json(result)
                }
            })
        },
        delete: function(req, res) {
            
        }
    }

    return UsersController;
}
module.exports = function(app) {
    let usersController = app.controllers.users;

    app.get('/users', usersController.fetchAll);
    app.post('/users', usersController.save);
    app.delete('/users/:id', usersController.delete);
};
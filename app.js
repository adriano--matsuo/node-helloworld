const app = require('express')();
const consign = require('consign');
const bodyParser = require('body-parser');

app.set('json spaces', 4);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

consign()
  .include('controllers')
  .then('routes')
  .then('models')
  .then('config')
.into(app);

app.listen(3000, function() {
    console.log('Servidor está rodando na porta 3000');
});
module.exports = function(app) {
    let config = {
        connect: function() {
            let mysql = require('mysql')

            let connection = mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: '',
                database: 'auladb'
            });

            connection.connect()

            return connection;
        }
    }

    return config;
}